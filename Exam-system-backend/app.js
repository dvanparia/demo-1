const express = require('express');

const studentRouter = require('./routes/student');
const testRouter = require('./routes/test');
const resultRouter = require('./routes/result');
const answerRouter = require('./routes/answer');

const app = express();

app.use(express.json());
app.use(studentRouter);
app.use(testRouter);
app.use(resultRouter);
app.use(answerRouter);

app.listen(process.env.PORT, () => {
    console.log('Server is up on port '+process.env.PORT);
})