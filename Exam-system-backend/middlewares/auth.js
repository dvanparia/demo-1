const jwt = require('jsonwebtoken');
const { pool } = require('../db/connect');

const auth = async (req,res,next) => {
    const token = req.header('Authorization').replace('Bearer ','');
    const decoded = jwt.verify(token, process.env.SECRET);
    const id = decoded.data;
    const client = await pool.connect();
    try{
        const response = await client.query("select * from student where enr_no='" + id + "'")
        if (response.rows.length == 0) {
            throw new Error();
        }
        req.id = id;
        next();
    } catch(error) {
        res.send('Please Authenticate Yourself')
    } finally {
        client.release();
    }
}

module.exports = auth;
