const express = require('express');
const auth = require('../middlewares/auth');
const { pool } = require('../db/connect');

const router = express.Router();

router.post('/api/answers', auth, async (req, res) => {
    const testId = req.body.testId;
    const questionIds = req.body.questionIds;
    const answers = req.body.answers;
    const client = await pool.connect();
    try {
        for(i = 0; i < questionIds.length; i++ ) {
            let response = await client.query("insert into studentanswers values("+testId+","+questionIds[i]+",'"+answers[i]+"')");
        }
        res.send(true);
    } catch(error) {
        console.log(error);
        res.send(true);
    } finally {
        client.release();
    }
})

module.exports = router;