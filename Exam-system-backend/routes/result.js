const express = require('express');
const auth = require('../middlewares/auth');
const { pool } = require('../db/connect');

const router = express.Router();

router.get('/api/results', auth, async (req, res) => {
    const id = req.id;
    const client = await pool.connect();
    try {
        const resultResponse = await client.query("select test.test_code,test_name,date_taken,marks_obtained,test_total_marks from studenttest inner join test on studenttest.test_code=test.test_code where enr_no='"+id+"' and is_taken=TRUE");
        let resultData = [];
        resultResponse.rows.forEach(function (row) {
            resultData.push({
                testCode: row.test_code,
                testName: row.test_name,
                dateTaken: Date.parse(row.date_taken),
                marksObtained: row.marks_obtained,
                totalMarks: row.test_total_marks
            });
        });
        res.send(resultData);
    } finally {
        client.release;
    }
})

router.post('/api/result', async (req, res) => {
    const id = +req.body.testId;
    const marksObtained = +req.body.marksObtained;
    const client = await pool.connect();
    try {
        const response = await client.query("update studenttest set is_taken=TRUE, marks_obtained="+marksObtained+" where test_id="+id+"");
        res.send(true);
    } finally {
        client.release();
    }
})

module.exports = router;