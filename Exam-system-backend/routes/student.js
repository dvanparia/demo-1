const express = require('express');
const { pool } = require('../db/connect');
const jwt = require('jsonwebtoken');

const router = new express.Router()

router.post('/api/login', async (req, res) => {
    const userid = req.body.user.userid;
    const password = req.body.user.password;
    let token = await jwt.sign({ data: userid }, process.env.SECRET);
    let error = null;
    const client = await pool.connect();
    try {
        const response = await client.query("SELECT * FROM student WHERE enr_no='"+userid+"'");
        if (response.rows.length == 0) {
            error = 'User Id does not exist';
            token = null;
        } else {
            if (password !== response.rows[0].password) {
                error = 'Incorrect Password';
                token = null;
            } else {
                req.body.user.name = response.rows[0].student_name;
                req.body.user.email = response.rows[0].email;
                error = null;
            }
        }
        const user = req.body.user;
        res.send({
            data: user,
            token,
            error
        });
    } finally {
        client.release();
    }
})

module.exports = router;