const express = require('express');
const { pool } = require('../db/connect');
const jwt = require('jsonwebtoken');
const auth = require('../middlewares/auth');

const router = express.Router();

router.get('/api/tests', auth, async (req, res) => {
    const id = req.id;
    const client = await pool.connect();
    try {
        const response = await client.query("select test_id,test.test_code,test_name,test_duration,test_date,test_total_marks from test inner join studenttest on test.test_code=studenttest.test_code where enr_no='"+id+"' and is_taken=FALSE");
        let testResponse = [];
        response.rows.forEach(function (row) {
            testResponse.push({
                testId: row.test_id,
                testCode: row.test_code,
                testName: row.test_name,
                testDuration: row.test_duration,
                testDate: Date.parse(row.test_date),
                testMarks: row.test_total_marks
            });
        });
        res.send(testResponse);
    } finally {
        client.release();
    }
});

router.get('/api/test', auth, async (req, res) => {
    const id = req.id;
    const test_id = +req.param('testId');
    const client = await pool.connect();
    try {
        const testResponse = await client.query("select test_id,test.test_code,test_name,test_duration,test_date,test_total_marks from test inner join studenttest on test.test_code=studenttest.test_code where test_id="+test_id+" and is_taken=FALSE");
        const testData = {
            testId: +testResponse.rows[0].test_id,
            testCode: testResponse.rows[0].test_code,
            testName: testResponse.rows[0].test_name,
            testDuration: +testResponse.rows[0].test_duration,
            testDate: Date.parse(testResponse.rows[0].test_date),
            testMarks: +testResponse.rows[0].test_total_marks
        };
        const questionsResponse = await client.query("select question.question_id,question_text,question_marks from testquestion inner join question on testquestion.question_id=question.question_id where test_code='"+testData.testCode+"'");
        let questionData = [];
        questionsResponse.rows.forEach(async function (row) {
            const id = +row.question_id;
            const optionsResponse = await client.query("select * from option where question_id="+id+"");
            let optionData = [];
            optionsResponse.rows.forEach(function (row) {
                optionData.push({
                    optionNo: row.option_no,
                    optionText: row.option_text,
                    isAnswer: row.is_answer,
                    isSelected: false
                });
            });
            questionData.push({
                questionId: +row.question_id,
                questionText: row.question_text,
                questionMarks: +row.question_marks,
                options: optionData
            });
        });
        setTimeout(() => {
            res.send({
                test: testData,
                questions: questionData
            });
        }, 100);
    } finally {
        client.release();
    }
})

module.exports = router;