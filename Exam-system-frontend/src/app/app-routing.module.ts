import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuardService } from './shared/auth-guard.service';
import { ResultListComponent } from './result-list/result-list.component';
import { TestComponent } from './test/test.component';
import { CanDeactivateGuardService } from './test/can-deactivate-guard.service';
import { TestGuardService } from './test/test-guard.service';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuardService],
  },
  {path: 'login', component: LoginComponent},
  {
    path: 'test/:id',
    component: TestComponent,
    canActivate: [AuthGuardService, TestGuardService],
    canDeactivate: [CanDeactivateGuardService]
  },
  {
    path: 'results',
    component: ResultListComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
