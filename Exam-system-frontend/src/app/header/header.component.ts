import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { TestService } from '../home/test.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  testMode: boolean;
  testSubscription: Subscription;

  constructor(private authService: AuthService, private testService: TestService) { }

  ngOnInit(): void {
    this.testSubscription = this.testService.testMode.subscribe(value => {
      this.testMode = value;
    });
  }

  onLogOut(): void {
    this.authService.logout();
  }

  ngOnDestroy(): void {
    this.testSubscription.unsubscribe();
  }
}
