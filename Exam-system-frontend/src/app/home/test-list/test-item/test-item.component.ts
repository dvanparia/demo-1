import { Component, OnInit, Input } from '@angular/core';
import { Test } from '../../test.model';
import { TestService } from '../../test.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-test-item',
  templateUrl: './test-item.component.html',
  styleUrls: ['./test-item.component.scss']
})
export class TestItemComponent implements OnInit {
  @Input() test: Test;
  @Input() index: number;
  tdate: Date;
  allowTest = false;

  constructor(private testService: TestService, private router: Router) { }

  ngOnInit(): void {
    this.tdate = new Date(this.test.testDate);
    const diff = this.tdate.getTime() - new Date().getTime();
    if (diff <= 0) {
      this.allowTest = true;
    }
    setTimeout(() => {
      this.allowTest = true;
    }, diff);
  }

  onTest(): void {
    this.testService.testMode.next(true);
    localStorage.setItem('testMode', 'true');
    this.router.navigate(['test', this.test.testId]);
  }
}
