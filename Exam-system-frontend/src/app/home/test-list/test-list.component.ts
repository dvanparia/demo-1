import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AuthService } from 'src/app/shared/auth.service';
import { User } from 'src/app/shared/user.model';
import { TestService } from '../test.service';
import { Test } from '../test.model';

@Component({
  selector: 'app-test-list',
  templateUrl: './test-list.component.html',
  styleUrls: ['./test-list.component.scss']
})
export class TestListComponent implements OnInit, OnDestroy {
  private userSubscription: Subscription;
  public user: User;
  public tests: Test[];
  dateToday = new Date();

  constructor(private authService: AuthService, private testService: TestService) {
    setInterval(() => {
      this.dateToday = new Date();
    }, 1);
  }

  ngOnInit(): void {
    this.userSubscription = this.authService.user.subscribe(user => {
      this.user = user;
    });
    this.testService.getTests().subscribe(tests => {
      this.tests = tests;
    });
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
