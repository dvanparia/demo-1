export class Test {
    constructor(
        public testId: number,
        public testCode: string,
        public testName: string,
        public testDuration: number,
        public testDate: string,
        public testMarks: number
    ) {}
}
