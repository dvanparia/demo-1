import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

import { Test } from './test.model';
import { TestData } from '../shared/testData';

@Injectable({ providedIn: 'root' })
export class TestService {
    test = new BehaviorSubject<TestData> (null);
    testMode = new BehaviorSubject<boolean> (false);

    constructor(private http: HttpClient, private router: Router) { }

    getTests(): Observable<Test[]> {
        return this.http.get<Test[]>('/api/tests');
    }

    getTestData(id: string): Observable<TestData> {
        return this.http.get<TestData>('/api/test', {params: new HttpParams().set('testId', id)});
    }

    storeAnswers(testId: number, questionIds: number[], answers: string[]): void {
        this.http.post<boolean>('/api/answers', { testId, questionIds, answers }).subscribe(response => {
            console.log(response);
        });
    }

    storeResult(testId: number, marksObtained: number): void {
        this.http.post<boolean>('/api/result', { testId, marksObtained }).subscribe(response => {
            if (response) {
                this.testMode.next(false);
                localStorage.removeItem('testMode');
                this.router.navigate(['home']);
            }
        });
    }
}
