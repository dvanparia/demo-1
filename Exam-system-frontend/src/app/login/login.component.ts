import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { User } from '../shared/user.model';
import { UserService } from '../shared/user.service';
import { AuthService } from '../shared/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(
    private userService: UserService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    if (this.authService.autoLogin()) {
      this.router.navigate(['home']);
    }
  }

  onLogIn(form: NgForm): void {
    const value = form.value;
    const user = new User(value.userid, null, null, value.password);
    this.userService.userLogIn(user).subscribe(response => {
      if (!response.error) {
        this.authService.authenticate(response);
        this.router.navigate(['home']);
      }
    });
  }

}
