import { Component, OnInit } from '@angular/core';

import { ResultListService } from './result-list.service';
import { Result } from './result.model';

@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.scss']
})
export class ResultListComponent implements OnInit {
  results: Result[];
  displayedColumns: string[];

  constructor(private resultListService: ResultListService) { }

  ngOnInit(): void {
    this.resultListService.getResults().subscribe(results => {
      this.results = results;
    });
    this.displayedColumns = ['testCode', 'testName', 'dateTaken', 'marksObtained', 'totalMarks'];
  }
}
