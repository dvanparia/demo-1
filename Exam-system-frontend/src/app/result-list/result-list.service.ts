import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Result } from './result.model';

@Injectable({ providedIn: 'root' })
export class ResultListService {

    constructor(private http: HttpClient) { }

    getResults(): Observable<Result[]> {
        return this.http.get<Result[]>('/api/results');
    }
}
