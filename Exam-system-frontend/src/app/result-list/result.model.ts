export class Result {
    constructor(
        public testCode: string,
        public testName: string,
        public dateTaken: string,
        public marksObtained: number,
        public totalMarks: number
    ) { }
}
