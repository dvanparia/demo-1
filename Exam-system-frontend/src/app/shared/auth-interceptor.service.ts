import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const userData = JSON.parse(localStorage.getItem('userdata'));
        if (!userData) {
            return next.handle(request);
        }
        const modifiedRequest = request.clone({
            headers: request.headers.set('Authorization', `Bearer ${userData.responseData.token}`)
        });
        return next.handle(modifiedRequest);
    }
}
