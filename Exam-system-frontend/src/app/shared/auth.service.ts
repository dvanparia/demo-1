import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from './user.model';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthService {
    user = new BehaviorSubject<User> (null);
    tokenExpirationTimer: any;
    tokenExpirationDate: number;

    constructor(private router: Router) {}

    authenticate(responseData): void {
        const user = responseData.data;
        this.user.next(user);
        this.autoLogout(60 * 60 * 12 * 1000);
        this.tokenExpirationDate = new Date().getTime() + 60 * 60 * 12 * 1000;
        localStorage.setItem('userdata', JSON.stringify({responseData, expirationDate: this.tokenExpirationDate}));
    }

    autoLogin(): boolean {
        const userData = JSON.parse(localStorage.getItem('userdata'));

        if (!userData) {
            return false;
        }

        if (userData.responseData.token) {
            console.log(userData.responseData.data);
            this.user.next(userData.responseData.data);
            const expirationDuration = new Date(userData.expirationDate).getTime() - new Date().getTime();
            this.autoLogout(expirationDuration);
        }
        return true;
    }

    logout(): void {
        this.user.next(null);
        this.router.navigate(['/login']);
        localStorage.clear();
        if (this.tokenExpirationTimer) {
            clearTimeout(this.tokenExpirationTimer);
        }
        this.tokenExpirationTimer = null;
    }

    autoLogout(expirationDuration: number): void {
        this.tokenExpirationTimer = setTimeout(() => {
            this.logout();
        }, expirationDuration);
    }

}
