export class Option {
    constructor(public optionNo: string, public optionText: string, public isAnswer: boolean, public isSelected: boolean) { }
}
