import {Option} from './option.model';

export class Question {
    constructor(public questionId: number, public questionText: string, public questionMarks: number, public options: Option[]) { }
}
