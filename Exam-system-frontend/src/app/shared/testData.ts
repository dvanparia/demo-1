import { Test } from '../home/test.model';
import { Question } from './question.model';

export interface TestData {
    test: Test;
    questions: Question[];
}
