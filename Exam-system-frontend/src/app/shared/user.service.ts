import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from './user.model';
import { Observable } from 'rxjs';

interface LoginResponse {
    data: User;
    token: string;
    error: string;
}

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) {}

    userLogIn(user: User): Observable<LoginResponse> {
        return this.http.post<LoginResponse>('/api/login', { user });
    }
}
