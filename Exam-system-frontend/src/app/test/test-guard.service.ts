import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { TestService } from '../home/test.service';

@Injectable({ providedIn: 'root' })
export class TestGuardService implements CanActivate {
    constructor(private router: Router, private testService: TestService) {}

    canActivate(route: ActivatedRouteSnapshot, router: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean> | UrlTree {
        if (localStorage.getItem('testMode')) {
            return true;
        }
        return this.router.createUrlTree(['/home']);
    }
}
