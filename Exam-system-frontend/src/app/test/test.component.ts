import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, UrlTree } from '@angular/router';
import { TestService } from '../home/test.service';
import { Test } from '../home/test.model';
import { Question } from '../shared/question.model';
import { Option } from '../shared/option.model';
import { NgForm } from '@angular/forms';
import { CanComponentDeactivate } from './can-deactivate-guard.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit, CanComponentDeactivate {
  id: string;
  test: Test;
  questions: Question[];
  options: Option[];
  testStartTime: number;
  testEndTime: number;
  timeRemaining: string;
  answers = [];
  questionIds = [];
  testAnswers = [];
  marksObtained = 0;
  config: any;
  changesSaved = false;
  optionSelected = [];
  @ViewChild('TestForm', {static: true}) testForm: NgForm;

  constructor(private route: ActivatedRoute, private testService: TestService, private router: Router) { }

  ngOnInit(): void {
    this.testService.testMode.next(true);
    this.id = this.route.snapshot.paramMap.get('id');
    this.testService.getTestData(this.id).subscribe(testData => {
      this.test = testData.test;
      this.questions = testData.questions;
      this.config = {
        itemsPerPage: 1,
        currentPage: 1,
        totalItems: this.questions.length
      };
      this.questions.forEach(question => {
        this.optionSelected.push('');
      });
      this.startTimer();
    });
  }

  startTimer(): void {
    this.testStartTime = new Date().getTime();
    this.timeRemaining = this.parseTime(this.test.testDuration * 60);
    this.testEndTime = this.testStartTime + this.test.testDuration * 60 * 1000;
    setInterval(() => {
      const now = new Date().getTime();
      const diff = (this.testEndTime - now) / 1000;
      if (diff === 0) {
        this.onSubmitTest(this.testForm);
      }
      this.timeRemaining = this.parseTime(diff);
    }, 1000);
  }

  parseTime(totalSeconds: number): string{
    let mins: string | number = Math.floor(totalSeconds / 60);
    let secs: string | number = Math.round(totalSeconds % 60);
    mins = (mins < 10 ? '0' : '') + mins;
    secs = (secs < 10 ? '0' : '') + secs;
    return `${mins}:${secs}`;
  }

  onSubmitTest(testForm: NgForm): void {
    this.answers = this.optionSelected;
    this.questions.forEach(question => {
      const id = '' + question.questionId + '';
      this.questionIds.push(question.questionId);
    });
    this.testService.storeAnswers(this.test.testId, this.questionIds, this.answers);
    this.calculateResult();
  }

  calculateResult(): void {
    this.questions.forEach(question => {
      question.options.forEach(option => {
        if (option.isAnswer) {
          this.testAnswers.push(option.optionNo);
        }
      });
    });
    for (const i in this.questions) {
      if (this.answers[i] === this.testAnswers[i]) {
        this.marksObtained += this.questions[i].questionMarks;
      }
    }
    this.changesSaved = true;
    this.testService.storeResult(this.test.testId, this.marksObtained);
  }

  pageChanged(event): void {
    this.config.currentPage = event;
  }

  canDeactivate(): boolean | Promise<boolean> | Observable<boolean> {
    if (!this.changesSaved) {
      alert('Submit the test before leaving the page');
      return;
    }
    return true;
  }

  onSelect(index: number, optionIndex: number, selectedOption: Option): void {
    this.optionSelected[index - 1] = selectedOption.optionNo;
    this.questions[index - 1].options.forEach(option => {
      if (option.optionNo !== selectedOption.optionNo) {
        option.isSelected = false;
      } else {
        option.isSelected = true;
      }
    });
  }
}
